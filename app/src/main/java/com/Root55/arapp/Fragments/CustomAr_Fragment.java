package com.Root55.arapp.Fragments;

        import com.Root55.arapp.MainActivity;
        import com.google.ar.core.Config;
        import com.google.ar.core.Session;
        import com.google.ar.sceneform.ux.ArFragment;

public class CustomAr_Fragment extends ArFragment {
    @Override
    protected Config getSessionConfiguration(Session session) {
        Config config = new Config(session);
        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
        config.setFocusMode(Config.FocusMode.AUTO);
        session.configure(config);


        this.getArSceneView().setupSession(session);



        ((MainActivity) getActivity()).setUpDataBase(config, session);

        return config;
    }

}
