package com.Root55.arapp;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.Root55.arapp.helpers.CameraPermissionHelper;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class StartScreen extends AppCompatActivity implements View.OnClickListener {
    private boolean mUserRequestedInstall = true;
    Session mSession;
    Button mArButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
        mArButton = findViewById(R.id.ar_btn);
   //     maybeEnableArButton();
        mArButton.setOnClickListener(this);

    }

//    void maybeEnableArButton() {
//        ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(this);
//        if (availability.isTransient()) {
//            // Re-query at 5Hz while compatibility is checked in the background.
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    maybeEnableArButton();
//                }
//            }, 200);
//        }
//        if (availability.isSupported()) {
//            mArButton.setVisibility(View.VISIBLE);
//            mArButton.setEnabled(true);
//            // indicator on the button.
//        } else { // Unsupported or unknown.
//            mArButton.setVisibility(View.INVISIBLE);
//            mArButton.setEnabled(false);
//        }
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ar_btn:

                startActivity(new Intent(StartScreen.this, MainActivity.class));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // ARCore requires camera permission to operate.
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            CameraPermissionHelper.requestCameraPermission(this);
            return;
        }
        try {
            if (mSession == null) {
                switch (ArCoreApk.getInstance().requestInstall(this, mUserRequestedInstall)) {
                    case INSTALLED:
                        // Success, create the AR session.
                        mSession = new Session(this);
                        break;
                    case INSTALL_REQUESTED:
                        // Ensures next invocation of requestInstall() will either return
                        // INSTALLED or throw an exception.
                        mUserRequestedInstall = false;
                        return;
                }
            }
        } catch (UnavailableUserDeclinedInstallationException e) {
            // Display an appropriate message to the user and return gracefully.
            Toast.makeText(this, "TODO: handle exception " + e, Toast.LENGTH_LONG)
                    .show();
            return;
        } catch (Exception ex) {

            // Current catch statements.


            return;  // mSession is still null.
        }

    }

}

