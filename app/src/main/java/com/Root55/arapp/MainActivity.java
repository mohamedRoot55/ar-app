package com.Root55.arapp;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.Root55.arapp.Fragments.CustomAr_Fragment;
import com.google.ar.core.Anchor;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.TransformableNode;

import java.util.Collection;
public class MainActivity extends AppCompatActivity implements Scene.OnUpdateListener {



    private CustomAr_Fragment mArFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        mArFragment = (CustomAr_Fragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        mArFragment.getArSceneView().getScene().addOnUpdateListener(this);
    }


    public void setUpDataBase(Config config, Session session) {
        Bitmap foxbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.arctic_fox);
        AugmentedImageDatabase aid = new AugmentedImageDatabase(session);
        aid.addImage("fox", foxbitmap);
        config.setAugmentedImageDatabase(aid);
    }

    @Override
    public void onUpdate(FrameTime frameTime) {
        Frame frame = mArFragment.getArSceneView().getArFrame();
        Collection<AugmentedImage> images = frame.getUpdatedTrackables(AugmentedImage.class);
        for (AugmentedImage image : images) {
            if (image.getTrackingState() == TrackingState.TRACKING) {
                if (image.getName().equals("fox")) {
                    Anchor anchor = image.createAnchor(image.getCenterPose());

                    CreateModel(anchor);
                }
            }
        }
    }

    private void CreateModel(Anchor anchor) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ModelRenderable.builder().setSource(this, Uri.parse("ArcticFox_Posed.sfb"))
                    .build().thenAccept(modelRenderable -> PlaceModel(modelRenderable, anchor));
        }

    }

    private void PlaceModel(ModelRenderable modelRenderable, Anchor anchor) {
        AnchorNode anchorNode = new AnchorNode(anchor);

        anchorNode.setRenderable(modelRenderable);
        mArFragment.getArSceneView().getScene().onAddChild(anchorNode);
    }



}